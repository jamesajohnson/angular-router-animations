import { Component, OnInit } from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

@Component({
    selector: 'app-child-one',
    templateUrl: './child-one.component.html',
    styleUrls: ['./child-one.component.css'],
    host: {
        '[@routeAnimation]': 'true',
        '[style.display]': "'block'",
        '[style.position]': "'absolute'"
    },
    animations: [
        trigger('routeAnimation', [
            state('*', style({transform: 'translateX(0)', opacity: 1})),
            transition('void => *', [
                style({transform: 'translateX(-100%)', opacity: 0}),
                animate(1000)
            ]),
            transition('* => void', animate(1000, style({transform: 'translateX(100%)', opacity: 0})))
        ])
    ]
})
export class ChildOneComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
