import { RouterAnimationsPage } from './app.po';

describe('router-animations App', () => {
  let page: RouterAnimationsPage;

  beforeEach(() => {
    page = new RouterAnimationsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
